import numpy as np
import threading
import multiprocessing

class CountThread(multiprocessing.Process):
   def __init__ (self,filename):
      multiprocessing.Process.__init__(self)
      self.q = multiprocessing.Queue()
      self.filename = filename
      self.sumall=0
      self.sumsq=0
      self.nbelems=0
       
   def run(self):
      print "loading file "+self.filename
      X=np.loadtxt(self.filename)
      self.sumall = ?? sum of all elements of X
      self.sumsq  = ?? sum of all elements squared of X
      self.nbelems = ?? number of elements of X
      self.q.put(np.array([self.sumall, self.sumsq, self.nbelems]))

   
