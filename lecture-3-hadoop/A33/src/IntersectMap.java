import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;

	public class IntersectMap extends
			Mapper<LongWritable, Text, Text, Text> {

		private Text gId = new Text(); // graphId
		private Text srcDestPair = new Text(); // source, destination pair

		
		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {

                        StringTokenizer itr = new StringTokenizer(value.toString());

                        while (itr.hasMoreTokens()) {

                                // get the graph id
                                gId.set(itr.nextToken());
                                // setting the key as source and the value as the destination
                                //The source and the destination are delimited by ":" here
                                srcDestPair.set(itr.nextToken() + ":" + itr.nextToken());

                                //emit key, value pair from mapper
                                //here the key is the source:destination pair and the graph id is the value
                                context.write(srcDestPair, gId);
                        }
		}
	}

