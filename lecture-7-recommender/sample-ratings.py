import numpy as np
import scipy.linalg as la

m = np.array([[1,1,1,0,0],[3,3,3,0,0],[4,4,4,0,0],[5,5,5,0,0],[0,0,0,4,4],[0,0,0,5,5],[0,0,0,2,2]])

print " --- original matrix ---"
print m
(U,s,V) = la.svd(m)

# take only two singular values
rU = U[:,0:2]
rs = np.diag(s[0:2])
rV = V[0:2,:]

# print reconstructed matrix
print "--- reconstructed matrix ---"
print rU.dot(rs.dot(rV)) 

# new user
q = np.array([[4,0,0,0,0]])
print "\n\nnew user q: "+str(q)


# ----- example: finding recommendations -----
# map new user into concept space
print "\n--- recomendations example ----"
qc = q.dot(rV.T)

# map user in concepto space into movie space for recommended movies
recommended_to_q = qc.dot(rV)
print "recommendation vector to q: "+str(recommended_to_q)

# ----- example: find similar users through cosine distance -----
# note, cosine distance -1 opposite direction, 0 orthogonal, 1 same direction
# l2 norm of all users
print "\n--- cosine distance in user concept space ---"
rUl2 = np.sqrt((rU*rU).sum(axis=1))

# l2 norm of new user
ql2 = np.sqrt((q*q).sum())

# cosine distance of q to all users 
rU_cosdistance_to_q = (rU.dot(qc.T)/ql2)[:,0]
print rU_cosdistance_to_q

# ----- example find similar users through euclidean distance ----
print "\n--- euclidean distance in user concept space ---"
rU_eucldistance = np.sqrt(((rU-qc)**2).sum(axis=1))
print rU_eucldistance
print "\n--- euclidean distance in original space ---"
U_eucldistance = np.sqrt(((m*1.0-q)**2).sum(axis=1))
print U_eucldistance

print "-----"
print "note how distance in concept space captures better our intuition"
