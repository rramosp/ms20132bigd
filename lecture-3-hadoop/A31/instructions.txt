** Run the word count example in hadoop


0. make sure hadoop is in your PATH, if not:

   include hadoop binaries in your PATH
   export PATH=/opt/hadoop-1.2.1/bin:$PATH

   format HDFS space
   hadoop namenode -format

   start hadoop
   /opt/hadoop-1.2.1/bin/start-all.sh

   make sure it works, open in your browser:
   http://localhost:50030
   http://localhost:50070 

1. inspect and understand .java files and input files in texts/

2. compile source code

    rm -rf classes *jar; mkdir classes
    javac -classpath /opt/hadoop-1.2.1/hadoop-core-1.2.1.jar:/opt/hadoop-1.2.1/lib/commons-cli-1.2.jar -d classes *java

3. package compiled binaries

    jar -cvf myapp.jar -C classes/ .

4. copy input data into HDFS

   hadoop fs -put texts/ texts

   ** make sure data is in HDFS

   hadoop fs -ls texts

5. run map reduce program

   hadoop jar myapp.jar org.myorg.WordCount texts output

6. get output data and inspect it

   hadoop fs -get output output
