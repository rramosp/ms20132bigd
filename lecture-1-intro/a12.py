import numpy as np
import ct
import time
import glob

tstart = time.time()
threads=[]
r=[]

for fname in glob.glob("data*"):
   t=ct.CountThread(fname)
   t.start()
   threads.append(t)
   
for t in threads:
   t.join()
   r.append(t.q.get())

r = np.array(r)

# sum the columns of r
tr = ????

# --- use values in tr to compute 
n    = ??? total number of elements
mean = ??? sum of Xi / n
stdv = ??? sqrt of ( (sum of Xi^2/n)-(sum of Xi/n)^2 )
   
tend = time.time()
print "time = "+str(tend-tstart)
print "mean = "+str(mean)
print "stdv = "+str(stdv)
print "n    = "+str(r[2])

# ----------------
# repeat with:
#   4 files of 100k elems each
#   2 files of 200k elems each
#   1 files of 400k elems each
