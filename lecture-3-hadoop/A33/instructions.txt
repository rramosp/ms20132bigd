your MR task must list the edges shared by all 3 graphs

see problem description here:

http://hadooptutorial.wikispaces.com/Setting+up+your+first+Hadoop+program
http://hadooptutorial.wikispaces.com/The+First+MapReduce+graph+program


run example with:

hadoop fs -put intersectInput graphs/intersectInput
hadoop jar dist/myapp.jar Intersect graphs/intersectInput goutput
rm -rf output
hadoop fs -get goutput output/

